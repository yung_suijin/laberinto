using System;
using System.Collections;
using System.Collections.Generic;

namespace laberinto
{
    internal class Laberinto
    {
        public bool bandera;
        public int pasos, pasos2, pasos3;

        public void juego(int[,] laberinto)
        {
            int renglones = 6, columnas = 9, renglonEntrada = 0, columnaEntrada = 0, 
            renglonSalida = 0, columnaSalida = 0, puntoActualRenglon = 0, puntoActualColumna = 0;
            
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();

            for(int i = 0; i < renglones; i++)
                for(int j = 0; j < columnas; j++)
                    if (laberinto[i, j] == 2)
                    {
                        renglonEntrada = i;
                        columnaEntrada = j;
                    }
                    else if (laberinto[i, j] == 3)
                    {
                        renglonSalida = i;
                        columnaSalida = j;
                    }

            puntoActualRenglon = renglonEntrada;
            puntoActualColumna = columnaEntrada;

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);

                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);

                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);

                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);

                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);

                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);

                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);

                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);

                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);

                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);

                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);

                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);

                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);

                Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas);
                Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas);
                Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas);
                //pasos++;

                Console.WriteLine((bandera == true) ? $"Punto actual: {puntoActualRenglon},{puntoActualColumna}" : 
                    $"Punto actual: {puntoActualRenglon},{puntoActualColumna} -> Punto muerto.");
                
                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }

            if (puntoActualRenglon == renglonSalida && puntoActualColumna == columnaSalida)
            {
                int renglonIndex = 0;
                
                Console.WriteLine($"Punto actual: {puntoActualRenglon},{puntoActualColumna}");

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                Console.WriteLine($"\nNumero de pasos: {pasos}\n");

                var numeroRenglones = laberinto.GetLength(0);
                var numeroColumnas = laberinto.GetLength(1);

                Console.WriteLine("    0  1  2  3  4  5  6  7  8");
                Console.WriteLine("  ---------------------------");

                for (int renglon = 0; renglon < numeroRenglones; renglon++)
                {
                    Console.Write($"{renglonIndex} |");

                    for (int columna = 0; columna < numeroColumnas; columna++)
                        Console.Write(String.Format(" {0} ", laberinto[renglon, columna]));

                    Console.WriteLine();
                    renglonIndex++;
                }
            }
        }

        public void Arriba(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna,
            Stack pilaRenglones, Stack pilaColumnas)
        {
            if (puntoActualRenglon - 1 >= 0)
            {
                if (laberinto[puntoActualRenglon - 1, puntoActualColumna] == 0 ||
                        laberinto[puntoActualRenglon - 1, puntoActualColumna] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon - 1);
                    pilaColumnas.Push(puntoActualColumna);
                    bandera = true;
                }
            }
        }

        public void Derecha(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna, int columnas,
            Stack pilaRenglones, Stack pilaColumnas)
        {
            if (puntoActualColumna + 1 <= columnas)
            {
                if (laberinto[puntoActualRenglon, puntoActualColumna + 1] == 0 ||
                        laberinto[puntoActualRenglon, puntoActualColumna + 1] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon);
                    pilaColumnas.Push(puntoActualColumna + 1);
                    bandera = true;
                }
            }
        }

        public void Abajo(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna, int renglones,
            Stack pilaRenglones, Stack pilaColumnas)
        {
            if (puntoActualRenglon + 1 <= renglones)
            {
                if (laberinto[puntoActualRenglon + 1, puntoActualColumna] == 0 ||
                        laberinto[puntoActualRenglon + 1, puntoActualColumna] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon + 1);
                    pilaColumnas.Push(puntoActualColumna);
                    bandera = true;
                }
            }
        }

        public void Izquierda(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna, int columnas,
            Stack pilaRenglones, Stack pilaColumnas)
        {
            if (puntoActualColumna - 1 <= columnas)
            {
                if (laberinto[puntoActualRenglon, puntoActualColumna - 1] == 0 ||
                        laberinto[puntoActualRenglon, puntoActualColumna - 1] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon);
                    pilaColumnas.Push(puntoActualColumna - 1);
                    bandera = true;
                }
            }
        }
    }
}