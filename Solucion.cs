using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace prueba2
{
    internal class Solucion
    {
        public List<BusquedaPropiedades> salidaOptima = new List<BusquedaPropiedades>();
        public bool bandera;
        public int renglones = 11, columnas = 18, renglonEntrada = 0, columnaEntrada = 0,
            renglonSalida = 0, columnaSalida = 0, puntoActualRenglon = 0, puntoActualColumna = 0;
        public Direcciones direccion = new Direcciones();
        public ArregloLaberinto clase = new ArregloLaberinto();
        public List<Pasos> nuevoPaso = new List<Pasos>();

        public void Laberinto()
        {
            int[,] laberinto = new int[11, 18] 
            { 
                { 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }, 
                { 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1 }, 
                { 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1 }, 
                { 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1 }, 
                { 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1 },
                { 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1 }, 
                { 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                { 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1 },
                { 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1 },
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1 }
            };

            for (int i = 0; i < renglones; i++)
                for (int j = 0; j < columnas; j++)
                    if (laberinto[i, j] == 2)
                    {
                        renglonEntrada = i;
                        columnaEntrada = j;
                    }
                    else if (laberinto[i, j] == 3)
                    {
                        renglonSalida = i;
                        columnaSalida = j;
                    }

            puntoActualRenglon = renglonEntrada;
            puntoActualColumna = columnaEntrada;

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                solucion1(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion2(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion3(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion4(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion5(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion6(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion7(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion8(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion9(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion10(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion11(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion12(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion13(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion14(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion15(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion16(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion17(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion18(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion19(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion20(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion21(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion22(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion23(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);
                solucion24(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, laberinto, columnas, renglones);

                var menorNumeroPasos = salidaOptima.OrderByDescending(x => x.NumeroPasos).ToList();
                var caminoSeleccionado = menorNumeroPasos.Last();

                laberinto[caminoSeleccionado.PasoPuntoActualRenglon, caminoSeleccionado.PasoPuntoActualColumna] = 8;

                laberinto = laberintoTemp(laberinto);

                puntoActualRenglon = caminoSeleccionado.PasoPuntoActualRenglon;
                puntoActualColumna = caminoSeleccionado.PasoPuntoActualColumna;

                salidaOptima.Clear();
            }

            if (puntoActualRenglon == renglonSalida && puntoActualColumna == columnaSalida)
            {
                int renglonIndex = 0;
                int columnaIndex = 0;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                var numeroRenglones = laberinto.GetLength(0);
                var numeroColumnas = laberinto.GetLength(1);

                Console.Write("    ");

                for (int columna = 0; columna < numeroColumnas; columna++)
                {
                    Console.Write($"{columnaIndex}  ");
                    columnaIndex++;
                }

                Console.WriteLine();

               /*Console.WriteLine("    0  1  2  3  4  5  6  7  8");
                Console.WriteLine("  ---------------------------");*/

                for (int renglon = 0; renglon < numeroRenglones; renglon++)
                {
                    Console.Write($"{renglonIndex} |");

                    for (int columna = 0; columna < numeroColumnas; columna++)
                        Console.Write(String.Format(" {0} ", laberinto[renglon, columna]));

                    Console.WriteLine();
                    renglonIndex++;
                }
            }
        }

        public void solucion1(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Arriba, derecha, abajo, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion2(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida, 
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Arriba, derecha, izquierda, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion3(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Arriba, abajo, derecha, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion4(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Arriba, abajo, izquierda, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion5(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Arriba, izquierda, derecha, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion6(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Arriba, izquierda, abajo, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion7(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Derecha, abajo, izquierda, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion8(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Derecha, abajo, arriba, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion9(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Derecha, izquierda, abajo, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion10(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Derecha, izquierda, arriba, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion11(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Derecha, arriba, izquierda, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion12(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Derecha, arriba, abajo, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion13(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto,  int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Abajo, derecha, izquierda, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion14(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Abajo, derecha, arriba, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion15(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Abajo, izquierda, arriba, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion16(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Abajo, izquierda, derecha, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion17(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Abajo, arriba, izquierda, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion18(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Abajo, arriba, derecha, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion19(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Izquierda, derecha, arriba, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion20(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Izquierda, derecha, abajo, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion21(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Izquierda, abajo, arriba, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion22(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Izquierda, abajo, derecha, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion23(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Izquierda, arriba, derecha, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public void solucion24(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, int columnas, int renglones)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            int[,] temp = laberintoTemp(laberinto);

            int pasos = 0, contador = 0;
            string orden = "Izquierda, arriba, abajo, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(temp, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(temp, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(temp, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                if(temp[puntoActualRenglon, puntoActualColumna] == temp[renglonEntrada, columnaEntrada])
                    temp[puntoActualRenglon, puntoActualColumna] = 2;
                else if (temp[puntoActualRenglon, puntoActualColumna] == 8)
                            temp[puntoActualRenglon, puntoActualColumna] = 8;
                else
                    temp[puntoActualRenglon, puntoActualColumna] = 9;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());

                nuevoPaso.Add(new Pasos {PasoRenglon = puntoActualRenglon, PasoColumna = puntoActualColumna });
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            var primero = nuevoPaso.First();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador, PasoPuntoActualRenglon =  primero.PasoRenglon, PasoPuntoActualColumna = primero.PasoColumna });
            nuevoPaso.Clear();
        }

        public int[,] laberintoTemp(int[,] laberinto)
        {
            for (int i = 0; i < renglones; i++)
                for (int j = 0; j < columnas; j++)
                    if (laberinto[i, j] == 9)
                        laberinto[i, j] = 0;

            return laberinto;
        }
    }
}