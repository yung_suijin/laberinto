using System.Collections;

namespace prueba2
{
    internal class Direcciones
    {
        public bool Arriba(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna,
            Stack pilaRenglones, Stack pilaColumnas, bool bandera)
        {
            if (puntoActualRenglon - 1 >= 0)
            {
                if (laberinto[puntoActualRenglon - 1, puntoActualColumna] == 0 ||
                        laberinto[puntoActualRenglon - 1, puntoActualColumna] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon - 1);
                    pilaColumnas.Push(puntoActualColumna);
                    bandera = true;
                }
            }
            return bandera;
        }

        public bool Derecha(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna, int columnas,
            Stack pilaRenglones, Stack pilaColumnas, bool bandera)
        {
            if (puntoActualColumna + 1 <= columnas)
            {
                if (laberinto[puntoActualRenglon, puntoActualColumna + 1] == 0 ||
                        laberinto[puntoActualRenglon, puntoActualColumna + 1] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon);
                    pilaColumnas.Push(puntoActualColumna + 1);
                    bandera = true;
                }
            }
            return bandera;
        }

        public bool Abajo(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna, int renglones,
            Stack pilaRenglones, Stack pilaColumnas, bool bandera)
        {
            if (puntoActualRenglon + 1 <= renglones)
            {
                if (laberinto[puntoActualRenglon + 1, puntoActualColumna] == 0 ||
                        laberinto[puntoActualRenglon + 1, puntoActualColumna] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon + 1);
                    pilaColumnas.Push(puntoActualColumna);
                    bandera = true;
                }
            }
            return bandera;
        }

        public bool Izquierda(int[,] laberinto, int puntoActualRenglon, int puntoActualColumna, int columnas,
            Stack pilaRenglones, Stack pilaColumnas, bool bandera)
        {
            if (puntoActualColumna - 1 <= columnas)
            {
                if (laberinto[puntoActualRenglon, puntoActualColumna - 1] == 0 ||
                        laberinto[puntoActualRenglon, puntoActualColumna - 1] == 3)
                {
                    pilaRenglones.Push(puntoActualRenglon);
                    pilaColumnas.Push(puntoActualColumna - 1);
                    bandera = true;
                }
            }
            return bandera;
        }
    }
}