namespace prueba2
{
    internal class BusquedaPropiedades
    {
        public string OrdenBusqueda { get; set; }
        public int NumeroPasos { get; set; }
        public int PuntosMuertos { get; set; }
        public int PasoPuntoActualRenglon {get; set; }
        public int PasoPuntoActualColumna {get; set; }
    }

    internal class Pasos
    {
        public int PasoRenglon { get; set; }
        public int PasoColumna { get; set; }
    }
}