using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace prueba2
{
    internal class Combinaciones
    {
        public List<BusquedaPropiedades> salidaOptima = new List<BusquedaPropiedades>();
        public bool bandera;
        public int renglones = 7, columnas = 9, renglonEntrada = 0, columnaEntrada = 0,
        renglonSalida = 0, columnaSalida = 0, puntoActualRenglon = 0, puntoActualColumna = 0;
        public Direcciones direccion = new Direcciones();

        public void Laberinto(int[,] laberinto)
        {
            var pilaRenglones = new Stack();
            var pilaColumnas = new Stack();
            var clase = new ArregloLaberinto();

            for (int i = 0; i < renglones; i++)
                for (int j = 0; j < columnas; j++)
                    if (laberinto[i, j] == 2)
                    {
                        renglonEntrada = i;
                        columnaEntrada = j;
                    }
                    else if (laberinto[i, j] == 3)
                    {
                        renglonSalida = i;
                        columnaSalida = j;
                    }

            puntoActualRenglon = renglonEntrada;
            puntoActualColumna = columnaEntrada;

            int[,] juego = clase.laberinto();
            solucion1(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego2 = clase.laberinto();
            solucion2(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego2, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego3 = clase.laberinto();
            solucion3(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego3, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego4 = clase.laberinto();
            solucion4(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego4, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego5 = clase.laberinto();
            solucion5(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego5, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego6 = clase.laberinto();
            solucion6(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego6, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego7 = clase.laberinto();
            solucion7(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego7, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego8 = clase.laberinto();
            solucion8(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego8, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego9 = clase.laberinto();
            solucion9(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego9, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego10 = clase.laberinto();
            solucion10(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego10, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego11 = clase.laberinto();
            solucion11(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego11, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego12 = clase.laberinto();
            solucion12(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego12, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego13 = clase.laberinto();
            solucion13(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego13, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego14 = clase.laberinto();
            solucion14(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego14, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego15 = clase.laberinto();
            solucion15(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego15, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego16 = clase.laberinto();
            solucion16(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego16, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego17 = clase.laberinto();
            solucion17(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego17, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego18 = clase.laberinto();
            solucion18(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego18, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego19 = clase.laberinto();
            solucion19(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego19, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego20 = clase.laberinto();
            solucion20(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego20, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego21 = clase.laberinto();
            solucion21(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego21, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego22 = clase.laberinto();
            solucion22(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego22, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego23 = clase.laberinto();
            solucion23(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego23, pilaRenglones, pilaColumnas, columnas, renglones);

            int[,] juego24 = clase.laberinto();
            solucion24(puntoActualRenglon, renglonSalida, puntoActualColumna, columnaSalida, juego24, pilaRenglones, pilaColumnas, columnas, renglones);

            Console.WriteLine();

            foreach(var item in salidaOptima)
            {
                Console.WriteLine($"{item.NumeroPasos}, {item.OrdenBusqueda}, {item.PuntosMuertos}");
            }

            var menorNumeroPasos = salidaOptima.OrderByDescending(x => x.NumeroPasos).ToList();
            
            Console.WriteLine($"\nLa ruta mas optima es: {menorNumeroPasos.Last().OrdenBusqueda}");
        }

        public void solucion1(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Arriba, derecha, abajo, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                Console.WriteLine((bandera == true) ? $"Punto actual: {puntoActualRenglon},{puntoActualColumna}" : 
                    $"Punto actual: {puntoActualRenglon},{puntoActualColumna} -> Punto muerto.");

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }

            if (puntoActualRenglon == renglonSalida && puntoActualColumna == columnaSalida)
            {
                int renglonIndex = 0;
                
                Console.WriteLine($"Punto actual: {puntoActualRenglon},{puntoActualColumna}");

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                Console.WriteLine($"\nNumero de pasos: {pasos}\n");

                var numeroRenglones = laberinto.GetLength(0);
                var numeroColumnas = laberinto.GetLength(1);

                Console.WriteLine("    0  1  2  3  4  5  6  7  8");
                Console.WriteLine("  ---------------------------");

                for (int renglon = 0; renglon < numeroRenglones; renglon++)
                {
                    Console.Write($"{renglonIndex} |");

                    for (int columna = 0; columna < numeroColumnas; columna++)
                        Console.Write(String.Format(" {0} ", laberinto[renglon, columna]));

                    Console.WriteLine();
                    renglonIndex++;
                }
            }
            
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador});
        }

        public void solucion2(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Arriba, derecha, izquierda, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion3(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Arriba, abajo, derecha, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion4(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Arriba, abajo, izquierda, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion5(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Arriba, izquierda, derecha, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion6(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Arriba, izquierda, abajo, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion7(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Derecha, abajo, izquierda, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion8(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Derecha, abajo, arriba, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion9(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Derecha, izquierda, abajo, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion10(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Derecha, izquierda, arriba, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion11(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Derecha, arriba, izquierda, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion12(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Derecha, arriba, abajo, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion13(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Abajo, derecha, izquierda, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion14(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Abajo, derecha, arriba, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion15(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Abajo, izquierda, arriba, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion16(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Abajo, izquierda, derecha, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion17(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Abajo, arriba, izquierda, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion18(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Abajo, arriba, derecha, izquierda.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion19(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Izquierda, derecha, arriba, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion20(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Izquierda, derecha, abajo, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion21(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Izquierda, abajo, arriba, derecha.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion22(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Izquierda, abajo, derecha, arriba.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion23(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Izquierda, arriba, derecha, abajo.";

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }

        public void solucion24(int puntoActualRenglon, int renglonSalida, int puntoActualColumna, int columnaSalida,
            int[,] laberinto, Stack pilaRenglones, Stack pilaColumnas, int columnas, int renglones)
        {
            int pasos = 0, contador = 0;
            string orden = "Izquierda, arriba, abajo, derecha.";
            var direccion = new Direcciones();

            while (puntoActualRenglon != renglonSalida || puntoActualColumna != columnaSalida)
            {
                bandera = false;

                bandera = direccion.Izquierda(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Arriba(laberinto, puntoActualRenglon, puntoActualColumna, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Abajo(laberinto, puntoActualRenglon, puntoActualColumna, renglones, pilaRenglones, pilaColumnas, bandera);
                bandera = direccion.Derecha(laberinto, puntoActualRenglon, puntoActualColumna, columnas, pilaRenglones, pilaColumnas, bandera);
                pasos++;

                if(bandera == false)
                    contador++;

                laberinto[puntoActualRenglon, puntoActualColumna] = 8;

                puntoActualRenglon = Convert.ToInt32(pilaRenglones.Pop());
                puntoActualColumna = Convert.ToInt32(pilaColumnas.Pop());
            }
            pilaColumnas.Clear();
            pilaRenglones.Clear();

            salidaOptima.Add(new BusquedaPropiedades { NumeroPasos = pasos, OrdenBusqueda = orden, PuntosMuertos = contador });
        }
    }
}