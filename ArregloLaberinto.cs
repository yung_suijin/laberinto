namespace prueba2
{
    internal class ArregloLaberinto
    {
        public int[,] laberinto()
        {
            int[,] arregloLaberinto = new int[7, 9] 
            { 
                { 1, 2, 1, 1, 1, 1, 1, 1, 1 }, 
                { 1, 0, 0, 0, 0, 1, 0, 0, 1 }, 
                { 1, 0, 0, 0, 0, 0, 1, 0, 1 }, 
                { 1, 1, 1, 1, 1, 0, 1, 0, 1 }, 
                { 1, 0, 0, 1, 0, 0, 1, 0, 1 }, 
                { 1, 0, 3, 0, 0, 0, 0, 0, 1 },
                { 1, 1, 1, 1, 1, 1, 1, 1, 1 } 
            };

            return arregloLaberinto;
        }
    }
}